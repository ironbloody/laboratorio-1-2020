import random
# impresora matriz


def impresora(matriz):
    for i in range(15):
        for j in range(12):
            print('|{0:^3}|'.format(matriz[i][j]), end=' ')
        print()
# funcion para crear matriz


def crearmatriz():
    matriz = []
    for i in range(15):
        matriz.append([])
        for j in range(12):
            matriz[i].append(random.randint(-10, 10))

    impresora(matriz)
    num_menor(matriz)
    print("suma de las 5 primeras colmunas")
    suma_5_columnas(matriz)
    print("negativos")
    negativos(matriz)
# funcion para obtener el numero menor


def num_menor(matriz):
    print("numero menor")
    num_menor = matriz[0][0]
    for i in range(15):
        for j in range(12):
            if num_menor > matriz[i][j]:
                num_menor = matriz[i][j]
    print(num_menor)
# funcion para obtener la suma de las 5 primeras columnas


def suma_5_columnas(matriz):
    for i in range(5):
        columna = 0
        for j in range(15):
            columna += matriz[j][i]
        print(columna)
# funcion para contar los negativos de la 5 a la novena columna


def negativos(matriz):
    contador = 0
    for i in range(5, 9):
        for j in range(15):
            if matriz[j][i] < 0:
                contador += 1
    print(contador)


crearmatriz()
