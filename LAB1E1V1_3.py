tupla = (1, 5, 7, 6, 3, 7, 2)
a = list(tupla)
'''funcion para cambiar las posicion del primero por el
segundo, el segundo por el tercero, etc.'''


def ordenar_lista(a):
    print("Original")
    print(a)
    a[0] = a[1]
    a[1] = a[2]
    a[2] = a[3]
    a[3] = a[4]
    a[4] = a[5]
    a[5] = a[6]
    a[6] = a[0]
    print("Cambiada")
    print(a)
ordenar_lista(a)